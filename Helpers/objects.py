class Batch:
    def __init__(self, hits, workers, images, subs, reward, maxAssignments, title):
        self.hits = hits
        self.workers = workers
        self.images = images
        self.subs = subs
        self.reward = reward
        self.maxAssignments = maxAssignments
        self.title = title


class Hit:
    def __init__(self, hitID, typeID, labels, blocks, counts, image):
        self.hitID = hitID
        self.typeID = typeID
        self.labels = labels
        self.blocks = blocks
        self.counts = counts
        self.image = image


class Image:
    def __init__(self, url, height, width):
        self.url = url
        self.height = height
        self.width = width


class Worker:
    def __init__(self, workerID, submissions):
        self.workerID = workerID
        self.submissions = submissions
        self.correctAnswers = 0
        self.negativePoints = 0
        self.isMalicious = False

    def getSubmissionsMade(self):
        return len(self.submissions)

    def getAccuracy(self):
        return self.correctAnswers / self.getSubmissionsMade() if self.getSubmissionsMade() > 0 else 0.5

    def getMalicious(self):
        return self.negativePoints / self.getSubmissionsMade() if self.getSubmissionsMade() > 0 else 0

    def getCredits(self):
        credits = self.getAccuracy() - self.getMalicious()
        return credits if credits > 0.0 else 0.0


class Sub:
    def __init__(self, hit, workerID, name, objects):
        self.hit = hit
        self.workerID = workerID
        self.name = name
        self.objects = objects


class CombinedSub:
    def __init__(self, hits, workerIDs, name, objects):
        self.hits = hits
        self.workerIDs = workerIDs
        self.name = name
        self.objects = objects


class Box:
    def __init__(self, x0, y0, x1, y1, label, score):
        self.x0 = x0
        self.y0 = y0
        self.x1 = x1
        self.y1 = y1
        self.label = label
        self.score = score


class Keypoint:
    def __init__(self, x0, y0, label):
        self.x0 = x0
        self.y0 = y0
        self.label = label


class CSVRow:
    def __init__(self, name, light, floorType, color, labels, labelsOriginal, root, counts=[]):
        self.name = name
        self.light = light
        self.floorType = floorType
        self.color = color
        self.labels = labels
        self.labelsOriginal = labelsOriginal
        self.root = root
        self.counts = counts
