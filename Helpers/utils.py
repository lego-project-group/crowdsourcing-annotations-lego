import math

from Helpers.objects import Box, Keypoint
from Helpers.figureManager import histogramDTFigure

inf = 10000

def StringToListSimple(labels):
    e = eval(labels)
    if isinstance(e, int) or isinstance(e, str):
        return [e]
    return list(e)

def FindRow(rows, name):
    for row in rows:
        if name == row.name:
            return row

    return None

def PointInBox(point, box):
    return point.x0 >= box.x0 and point.x0 <= box.x1 and point.y0 >= box.y0 and point.y0 <= box.y1

def RemoveDuplicateBoxes(boxes, threshold):
    uniqueBoxes = []

    for i in range(0, len(boxes)):
        flag = True
        for j in range(i + 1, len(boxes)):
            if IoUboxes(boxes[i], boxes[j]) > threshold:
                flag = False

        if flag:
            uniqueBoxes.append(boxes[i])

    return uniqueBoxes

def MeanBox(boxes):
    x0, y0, x1, y1 = 0, 0, 0, 0
    for box in boxes:
        x0 += box.x0
        y0 += box.y0
        x1 += box.x1
        y1 += box.y1

    l = len(boxes)
    return Box(round(x0/l), round(y0/l), round(x1/l), round(y1/l), boxes[0].label, 0.0)

def MeanWeightedBox(boxes, weights, tc):
    if tc == 0:
        tc = len(weights)
        for i in range(0, len(weights)):
            weights[i] = 1.0

    x0, y0, x1, y1 = 0, 0, 0, 0
    for i in range(0, len(boxes)):
        box = boxes[i]
        weight = weights[i]

        x0 += box.x0 * weight
        y0 += box.y0 * weight
        x1 += box.x1 * weight
        y1 += box.y1 * weight

    return Box(round(x0/tc), round(y0/tc), round(x1/tc), round(y1/tc), boxes[0].label, 0.0)

def MeanIoUBatch(IoUs):
    sum = 0
    for IoU in IoUs:
        meanim = MeanIoU(IoU)
        sum += meanim

    return sum/len(IoUs)

def MeanIoUBatchDependentNBlocks(IoUs, n):
    sums = [0.0] * n
    counts = [0] * n

    for IoU in IoUs:
        meanim = MeanIoU(IoU)
        index = len(IoU)-1

        sums[index] += meanim
        counts[index] += 1

    for i in range(0, n):
        if counts[i] > 0:
            sums[i] = sums[i]/counts[i]

    return sums

def MeanIoU(IoUs):
    sum = 0
    for IoU in IoUs:
        sum += IoU[0]

    return sum/len(IoUs)

def MeanIoUImagesAbovethreshold(IoUs, threshold):
    sum = 0
    for IoU in IoUs:
        iouValue = MeanIoU(IoU)
        if iouValue > threshold:
            sum += 1

    return sum

def IoUBoxesAboveThreshold(IoUs, threshold):
    sum = 0
    for IoU in IoUs:
        sum += IoUBoxAbovethreshold(IoU, threshold)

    return sum

def IoUBoxAbovethreshold(IoUs, threshold):
    sum = 0
    for IoU in IoUs:
        if IoU[0] >= threshold:
            sum += 1

    return sum

def Center(box0):
    box0x = (box0.x1 - box0.x0) / 2 + box0.x0
    box0y = (box0.y1 - box0.y0) / 2 + box0.y0

    return box0x, box0y

def Dist(box0, box1):
    box0x, box0y = Center(box0)
    box1x, box1y = Center(box1)

    return math.sqrt((box1x - box0x) ** 2 + (box1y - box0y) ** 2)

def DistPoint(point, box):
    pointx = point.x0
    pointy = point.y0
    boxx, boxy = Center(box)

    return math.sqrt((boxx - pointx) ** 2 + (boxy - pointy) ** 2)

def ClosestBoxToPoint(point, aboxes):
    dist = inf
    other = None
    for j in range(0, len(aboxes)):
        abox = aboxes[j]
        ndist = DistPoint(point, abox)
        if DistPoint(point, abox) < dist:
            other = abox
            dist = ndist

    return other

def ClosestSimilairBox(gbox, aboxes):
    dist = inf
    other = None
    for j in range(0, len(aboxes)):
        abox = aboxes[j]
        if gbox.label == abox.label:
            ndist = Dist(gbox, abox)
            if Dist(gbox, abox) < dist:
                other = abox
                dist = ndist

    return other

def IoUBoxBatch(combineds, truths):
    ious = []
    scores = []

    for c in combineds:
        row = FindRow(truths, c.name)
        if row != None:
            iou, score = IoUboxSets(row.objects, c.objects, c.name)
            if iou != None:
                ious.append(iou)

            scores.append(score)

    iousDT, distancesDT = [], []

    if score != None and score != []:
        for i in range(len(ious)):
            iousDT.extend(iou[0] for iou in ious[i])
            distancesDT.extend(scores[i])

        histogramDTFigure(iousDT, distancesDT)

    return ious

def IoUboxSets(gboxes, aboxes, name):
    IoU = []
    score = []
    for i in range(0, len(gboxes)):
        gbox = gboxes[i]
        other = ClosestSimilairBox(gbox, aboxes)

        if other != None:
            iou = IoUboxes(gbox, other)
            if other.score:
                score.append(other.score)

            IoU.append((iou, name, Center(gbox), Center(other)))

            if Center(other) == (0.0, 0.0):
                return None, None

    return IoU, score

def IoUboxes(gbox, abox):
    widthGbox = abs(gbox.x1 - gbox.x0)
    heightGbox = abs(gbox.y1 - gbox.y0)
    AreaGbox = widthGbox * heightGbox

    widthAbox = abs(abox.x1 - abox.x0)
    heightAbox = abs(abox.y1 - abox.y0)
    AreaAbox = widthAbox * heightAbox

    widthIntersection = min(gbox.x0 + widthGbox, abox.x0 + widthAbox) - max(gbox.x0, abox.x0)
    heightIntersection = min(gbox.y0 + heightGbox, abox.y0 + heightAbox) - max(gbox.y0, abox.y0)
    AreaIntersection = widthIntersection * heightIntersection

    if widthIntersection <= 0 or heightIntersection <= 0:
        return 0

    return AreaIntersection / (AreaGbox + AreaAbox - AreaIntersection)

def PointToBox(keypoints, blocks):
    x0, y0, x1, y1 = inf, inf, 0, 0
    label = ""

    for k in keypoints:
        x = k["x"]
        y = k["y"]

        if (label != k["label"]) and label != "":
            return None

        label = k["label"]

        x0 = min(x0, x)
        y0 = min(y0, y)
        x1 = max(x1, x)
        y1 = max(y1, y)

    return Box(x0, y0, x1, y1, blocks[int(label.split()[1]) - 1], 0.0)

def PointToUnlabeledBox(keypoints):
    x0, y0, x1, y1 = inf, inf, 0, 0

    for k in keypoints:
        x = k["x"]
        y = k["y"]

        x0 = min(x0, x)
        y0 = min(y0, y)
        x1 = max(x1, x)
        y1 = max(y1, y)

    return Box(x0, y0, x1, y1, "None", 0.0)

def PointsToBoxes(keypoints, blocks, counts):
    boxes = []
    if keypoints == "[]":
        return [], "No points placed while there should have been."

    klist = list(eval(keypoints.strip('][').split(', ')[0]))

    count = 0

    if (len(klist) < 4):
        return [], "Less than 4 points placed in total while at least 4 should have been placed for a single block."

    if counts != []:
        label = int(klist[0]["label"].split()[1]) - 1
        labelsUsed = [label]
        for k in klist:
            l = int(k["label"].split()[1]) - 1

            if count == 4:
                if (l < label):
                    return [], "Points placed out of order with labels: brick " + str(label + 1) + " and brick " + str(l + 1) + ". Labels should have been placed in the order provided."

                label = l
                labelsUsed.append(label)
                count = 0

            if l != label:
                return [], "Not 4 points placed with box with label: " + str(l) + " in sequence. While 4 points in sequence for each block is necessary."

            count += 1

        if count != 4:
            return [], "Not 4 points placed for the last block in the image."

        countsSub = [labelsUsed.count(item) for item in set(labelsUsed)]

        if  countsSub != counts:
            return [], "Not all labels used enough times your frequencies" + str(countsSub) + " should have been " + str(counts) + " ."

    for i in range(0, len(klist)-3, 4):
        box = PointToBox([klist[i], klist[i+1], klist[i+2], klist[i+3]], blocks)
        if box != None:
            boxes.append(box)
        else:
            return [], "Box is none."

    return boxes, ""

def PointsToUnlabeledBoxes(keypoints, blocks, counts):
    boxes = []
    if keypoints == "[]":
        return [], "No points placed while there should have been."

    klist = list(eval(keypoints.strip('][').split(', ')[0]))

    count = 0
    if (len(klist) < 4):
        return [], "Less than 4 points placed in total while at least 4 should have been placed for a single block."

    if (len(klist) < sum(counts) * 4):
        return [], "Less than 4 points placed for each block."

    if (len(klist) > sum(counts) * 4):
        return [], "More than 4 points placed for each block."

    for i in range(0, len(klist)-3, 4):
        box = PointToUnlabeledBox([klist[i], klist[i+1], klist[i+2], klist[i+3]])
        boxes.append(box)

    return boxes, ""


def PointToKeypoint(point, blocks):
    x0 = point["x"]
    y0 = point["y"]
    label = point["label"]

    return Keypoint(x0, y0, blocks[int(label.split()[1]) - 1])

def PointsToKeypoints(points, blocks, counts):
    keypoints = []
    feedback = ""
    klist = StringToListSimple(points)

    if (len(klist) < sum(counts)):
        return [], "Less than one point placed for each block."

    if (len(klist) > sum(counts)):
        return [], "More than one point placed for each block."

    for i in range(0, len(klist)):
        keypoint = PointToKeypoint(klist[i], blocks)
        keypoints.append(keypoint)

    return keypoints, feedback

def BoxesAbovethreshold(boxes, values, threshold):
    result = []
    for i in range(0, len(boxes)):
        if values[i] > threshold:
            result.append(boxes[i])

    if len(result) == 0:
        return boxes

    return result

def GetWorker(workerId, workers):
    for worker in workers:
        if workerId == worker.workerID:
            return worker

    return None

def IoUObjectsCount(subs):
    sum = 0

    for sub in subs:
        sum += len(sub)

    return sum

def BatchObjectsCount(subs):
    sum = 0

    for sub in subs:
        sum += len(sub.objects)

    return sum

def GetAboveThresholds(thresholds, batch, total):
    boxes = []
    images = []
    for t in thresholds:
        boxes.append(IoUBoxesAboveThreshold(batch, t) / total)
        images.append(MeanIoUImagesAbovethreshold(batch, t) / len(batch))

    return boxes, images