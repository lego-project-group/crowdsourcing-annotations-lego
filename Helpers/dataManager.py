import csv
import math
import random

from pathlib import Path
from Helpers.utils import PointsToBoxes,PointsToUnlabeledBoxes, PointsToKeypoints, FindRow, GetWorker
from Helpers.objects import Image, Hit, Worker, Batch, Sub, CSVRow

def ReadRows(roots, small, extra="", root2="\\CSV\\labels"):
    rows = []

    for root in roots:
        path = "Data\\" + root + root2 + extra + ".csv"
        with open(path, mode='r') as csvf:
            results = csv.DictReader(csvf)
            for line in results:
                if small:
                    row = CSVRow(line["Name"],
                                None,
                                None,
                                None,
                                line["Labels"],
                                line["Blocks"],
                                line["Image"],
                                counts=line["Counts"])
                else:
                    row = CSVRow(line["Name"],
                                line["Light"],
                                line["FloorType"],
                                line["Color"],
                                StringToList(line["Labels"]),
                                StringToListSimple(line["Labels"]),
                                root)

                rows.append(row)

    return rows

def CreateSets(sets, roots, groundtruths):

    rows = ReadRows(roots, False)

    # Draw random sets
    randomItems = random.sample(range(0, len(rows)), len(rows))
    index = math.floor(len(randomItems)/sets)

    # Write to sets
    for i in range(1, sets + 1):
        path = "Data\\Sets\\crowdsourceSets\\crowdsourceSet" + str(i) +"\\CSV\\labels.csv"
        WriteRange(rows, randomItems, (i - 1) * index, i * index, path)

    # Write all combined
    path = "Data\\Sets\\crowdsourceSets\\crowdsourceSetCombined\\CSV\\labels.csv"
    WriteRange(rows, randomItems, 0, len(rows), path)

    # Write groundtruth sets
    amount = math.floor(index / groundtruths)
    allPicks = []
    for i in range(1, groundtruths + 1):
        randomPicks = random.sample(randomItems[((i - 1) * index):(i * index)], amount)
        allPicks.extend(randomPicks)
        path = "Data\\Sets\\groundtruthSets\\groundtruthSet" + str(i) +"\\CSV\\labels.csv"
        WriteRange(rows, randomPicks, 0, amount, path)

    # Write all groundtruths
    path = "Data\\Sets\\groundtruthSets\\groundtruthSetCombined\\CSV\\labels.csv"
    WriteRange(rows, allPicks, 0, len(allPicks), path)

def StringToList(labels):
    e = eval(labels.strip(']['))
    if isinstance(e, str):
        return [e]
    return list(dict.fromkeys(list(e)))

def StringToListSimple(labels):
    e = eval(labels.strip(']['))
    if isinstance(e, int) or isinstance(e, str):
        return [e]
    return list(e)

def GenerateLabels(i):
    s = "["
    for i in range(1, i+1):
        s += "\'brick " + str(i) + "\', "
    return s + "]"

def WriteRange(rows, randomItems, f, t, path):
    with open(path, mode='w+', newline='') as csvf:
        writer = csv.writer(csvf)
        writer.writerow(["Name", "Labels", "Blocks", "Image"])

        for j in range(f, t):
            randomItem = randomItems[j]
            imagePath = "https://surfdrive.surf.nl/files/index.php/s/B7PEqFZSZZBPcW8/download?path=%2FData%2FInput%2FDatasets%2F" + rows[randomItem].root + "%2FImages&files=" + rows[randomItem].name + ".jpg"
            writer.writerow([rows[randomItem].name, GenerateLabels(len(rows[randomItem].labels)), rows[randomItem].labels, imagePath])

def SubsToCSV(subs, folder):
    index = 0
    Path("Data\\Output\\" + folder).mkdir(parents=True, exist_ok=True)
    for sub in subs:
        SubToCSV(sub, folder)
        index += 1

def ExtendedSubsToCSV(subs, rows, folder):
    index = 0
    Path("Data\\Output\\" + folder).mkdir(parents=True, exist_ok=True)
    for sub in subs:
        ExtendedSubToCSV(sub, FindRow(rows, sub.name), folder)
        index += 1

def CombinedSubsToCSV(subs, rows, folder):
    index = 0
    Path("Data\\Output\\" + folder).mkdir(parents=True, exist_ok=True)
    for sub in subs:
        CombinedSubToCSV(sub, FindRow(rows, sub.name), folder)
        index += 1

def SubToCSV(sub, folder):
    boxes = sub.objects
    image = sub.hit.image
    with open("Data\\Output\\" + folder + "\\" + sub.name + ".csv", mode='w+', newline='') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow([image.url, image.width, image.height])
        for box in boxes:
            writer.writerow([box.label, box.x0, box.y0, box.x1, box.y1])

def ExtendedSubToCSV(sub, row, folder):
    boxes = sub.objects
    image = sub.hit.image
    with open("Data\\Output\\" + folder +  "\\"+ sub.name + "_" + sub.workerID + ".csv", mode='w+', newline='') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow([image.url, image.width, image.height])
        writer.writerow([row.name, row.light, row.floorType, row.color, row.labelsOriginal])
        for box in boxes:
            writer.writerow([box.label, box.x0, box.y0, box.x1, box.y1])

def CombinedSubToCSV(sub, row, folder):
    boxes = sub.objects
    image = sub.hits[0].image
    with open("Data\\Output\\" + folder +  "\\"+ sub.name + ".csv", mode='w+', newline='') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow([image.url, image.width, image.height])
        writer.writerow([row.name, row.light, row.floorType, row.color, row.labelsOriginal])
        for box in boxes:
            writer.writerow([box.label, box.x0, box.y0, box.x1, box.y1])


def IoUToCSV(IoUs, name):
    with open("Data\\Output\\" + name + ".csv", mode='w+', newline='') as csv_file:
        writer = csv.writer(csv_file)
        for IoU in IoUs:
            writer.writerow([IoU[0], IoU[1][0], IoU[1][1], IoU[2][0], IoU[2][1]])

def ReadBatch(name, kind):
    print("Started reading: " + name)

    with open("Data\\Temp\\debug.csv", mode='w+', newline='') as debug_file:
        with open("Data\\Input\\Crowdsourcing\\" + name, mode='r') as csv_file:

            results = csv.DictReader(csv_file)
            writer = csv.DictWriter(debug_file, results.fieldnames)
            writer.writeheader()

            line_count = 0

            hits, workers, images, subs = [], [], [], []
            reward, maxAssignments, title = "", "", ""

            for row in results:
                if row["AssignmentStatus"] == "Rejected":
                    continue

                image = Image(row["Input.Image"], row["Answer.annotatedResult.inputImageProperties.height"],
                              row["Answer.annotatedResult.inputImageProperties.width"])

                blocks = StringToListSimple(row["Input.Blocks"])
                if "Input.Counts" in row:
                    counts = StringToListSimple(row["Input.Counts"])
                else:
                    counts = []

                if "Input.Labels" in row:
                    labels = StringToListSimple(row["Input.Labels"])
                else:
                    labels = []

                hit = Hit(row["HITId"], row["HITTypeId"], blocks, labels, counts, image)

                keypoints = row["Answer.annotatedResult.keypoints"]

                if kind == "box":
                    madeObs, feedback = PointsToBoxes(keypoints, blocks, counts)
                elif kind == "unlabeled_box":
                    madeObs, feedback = PointsToUnlabeledBoxes(keypoints, blocks, counts)
                elif kind == "keypoints":
                    madeObs, feedback = PointsToKeypoints(keypoints, blocks, counts)

                # Check if there is feedback for the worker.
                if feedback != "":
                    row["Reject"] = feedback
                    writer.writerow(row)
                    continue

                # Check if there is no feedback and approve submission.
                else:
                    row["Approve"] = "x"
                    #writer.writerow(row)

                sub = Sub(hit, row["WorkerId"], row["Input.Name"], madeObs)

                images.append(image)
                hits.append(hit)
                subs.append(sub)

                existWorker =  GetWorker(row["WorkerId"], workers)
                if existWorker != None:
                    existWorker.submissions.append(sub)
                else:
                    workers.append(Worker(row["WorkerId"], [sub]))

                reward = row["Reward"]
                maxAssignments = row["MaxAssignments"]
                title = row["Title"]

                line_count += 1

            batch = Batch(hits, workers, images, subs, reward, maxAssignments, title)
            print("Done reading " + name)

    return batch

def Difference(batch, csvPath, name, reverse=False):
    rows = ReadRows([csvPath], True)

    with open("Data\\Temp\\" + name + ".csv", mode='w+', newline='') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow(["Name", "Labels", "Blocks", "Counts", "Image"])

        if reverse:
            for row in rows:
                if FindRow(batch.subs, row.name):
                    writer.writerow([row.name, row.labels, row.labelsOriginal, row.counts, row.root])

        else:
            for row in rows:
                if not FindRow(batch.subs, row.name):
                    writer.writerow([row.name, row.labels, row.labelsOriginal, row.counts, row.root])

def AddCount(roots, csvPath, name, extra=""):
    rowsFile = ReadRows(roots, False, extra)
    rowsCsv = ReadRows([csvPath], True, extra)

    with open("Data\\Temp\\" + name + ".csv", mode='w+', newline='') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow(["Name", "Labels", "Blocks", "Counts", "Image"])
        for row in rowsCsv:
            count = []
            rowFile = FindRow(rowsFile, row.name)

            # Check if there exists a row with the given name.
            if rowFile != None:
                for lab in StringToListSimple(row.labelsOriginal):
                    count.append(rowFile.labelsOriginal.count(lab))

            writer.writerow([row.name, row.labels, row.labelsOriginal, count, row.root])

def AddQualification(workers, input):
    print("Started reading: " + input)

    with open("Data\\Temp\\workerOutput.csv", mode='w+', newline='') as output_file:
        with open("Data\\Input\\Workers\\" + input, mode='r') as input_file:
            rows = csv.DictReader(input_file)
            writer = csv.DictWriter(output_file, rows.fieldnames)
            writer.writeheader()

            for row in rows:
                worker = GetWorker(row["Worker ID"], workers)
                if worker != None:
                    row["UPDATE-Worked on draw task"] = 100
                    writer.writerow(row)

def ChangeStatus(crowdsourcing, tochange):

    changeRows = ReadRows(["\\Temp"], True, root2="\\"+tochange)

    with open("Data\\Temp\\changed.csv", mode='w+', newline='') as output_file:
        with open("Data\\Input\\Crowdsourcing\\" + crowdsourcing, mode='r') as input_file:
            rows = csv.DictReader(input_file)
            writer = csv.DictWriter(output_file, rows.fieldnames)
            writer.writeheader()

            for row in rows:
                r = FindRow(changeRows, row["Input.Name"])
                if r:
                    row["AssignmentStatus"] = "Rejected"

                writer.writerow(row)
