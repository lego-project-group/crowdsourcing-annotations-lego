import matplotlib.pyplot as plt

def histogramDTFigure(ious, distances):

    plt.suptitle('Distance v.s. quality in decomposing tasks method', fontsize=15)
    plt.hist2d(ious, distances, bins=(30, 30))
    plt.xlabel('IoU box')
    plt.ylabel('Pixels between click and center box')
    plt.colorbar()
    plt.show()

def qualityNBricksFigure(batches):
    plt.suptitle('Quality methods v.s. number of blocks', fontsize=15)
    plt.plot(range(1, 14), batches[0], label='Majority vote')
    plt.plot(range(1, 14), batches[1], label='Majority vote plus')
    plt.plot(range(1, 14), batches[2], label='Rejecting workers')
    plt.plot(range(1, 14), batches[3], label='Rejecting workers plus')
    plt.plot(range(1, 14), batches[4], label='Decomposing tasks')
    plt.xticks(range(1, 14))
    plt.yticks([x * 0.1 for x in range(0, 11)])
    plt.xlabel('Blocks per image')
    plt.ylabel('Quality of image in mIoU')
    plt.axis([1, 13, 0, 1])
    plt.grid(True)
    plt.legend()
    plt.show()

def qualityImagesFigure(thresholds, images):
    plt.suptitle('Quality images annotated by methods', fontsize=15)
    plt.plot(thresholds, images[0], label='Majority vote')
    plt.plot(thresholds, images[1], label='Majority vote plus')
    plt.plot(thresholds, images[2], label='Rejecting workers')
    plt.plot(thresholds, images[3], label='Rejecting workers plus')
    plt.plot(thresholds, images[4], label='Decomposing tasks')
    plt.xticks(thresholds)
    plt.xlabel('Threshold')
    plt.ylabel('Ratio of images above threshold')
    plt.axis([0, 1, 0, 1])
    plt.xticks([x * 0.1 for x in range(0, 11)])
    plt.yticks([x * 0.1 for x in range(0, 11)])
    plt.grid(True)
    plt.legend()
    plt.show()

def qualityBoxesFigure(thresholds, boxes):
    plt.suptitle('Quality box annotation of methods', fontsize=15)
    plt.plot(thresholds, boxes[0], label='Majority vote')
    plt.plot(thresholds, boxes[1], label='Majority vote plus')
    plt.plot(thresholds, boxes[2], label='Rejecting workers')
    plt.plot(thresholds, boxes[3], label='Rejecting workers plus')
    plt.plot(thresholds, boxes[4], label='Decomposing tasks')
    plt.xticks(thresholds)
    plt.xlabel('Threshold')
    plt.ylabel('Ratio of boxes above threshold')
    plt.axis([0, 1, 0, 1])
    plt.xticks([x * 0.1 for x in range(0, 11)])
    plt.yticks([x * 0.1 for x in range(0, 11)])
    plt.grid(True)
    plt.legend()
    plt.show()

def table2Figure(rows):
    table = plt.table(cellText=rows,
                      rowLabels=["Mean IoU", "Mean ratio correct image(mIoU > 0.5)", "Mean cost per image",
                                 "Mean IoU per dollar", "Mean correct image per dollar"],
                      colLabels=["Decomposing tasks", "Rejecting workers", "Rejecting workers plus", "Majority vote",
                                 "Majority vote plus"],
                      loc="center")

    plt.suptitle('Quality and cost of methods', fontsize=15)
    plt.axis("off")
    plt.grid(True)
    plt.tight_layout()
    table.set_fontsize(14)
    table.scale(1.5, 1.5)
    plt.show()

