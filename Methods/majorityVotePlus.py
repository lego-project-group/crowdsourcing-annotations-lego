from Helpers.utils import IoUboxes, MeanBox, BoxesAbovethreshold
from Helpers.objects import CombinedSub
from collections import defaultdict

# Run the improved majority vote for a batch.
def MajorityVoteBatch(batch, threshold):
    results = []

    # Order the submissions in groups with submissions on the same task.
    subTypes = defaultdict(list)
    for sub in batch.subs:
        subTypes[sub.name].append(sub)

    # For all distinct submissions do majority vote.
    for subs in subTypes.values():
        # When there are 1 or less submissions than skip this submission.
        if len(subs) <= 1:
            continue

        # Do improved majority vote and add to the results when not none.
        res = MajorityVoteSingle(subs, threshold)
        if res != None:
            results.append(res)

    return results

# Run the improved majority for a single image.
def MajorityVoteSingle(subs, threshold):
    majorityBoxes = []
    workerIDs = []
    hits = []
    name = ""
    obLen = -1

    # For all submissions on this single image.
    for sub in subs:

        # Add info for creating a combined sub object.
        workerIDs.append(sub.workerID)
        hits.append(sub.hit)
        name = sub.name

        # Check that the submissions have the same length of boxes.
        if obLen != -1 and obLen != len(sub.objects):
            return None

        obLen = len(sub.objects)

    # For all boxes.
    for i in range(0, len(subs[0].objects)):

        boxes = []
        miouBoxes = []

        # For all submission their box.
        for j in range(0, len(subs)):
            iouBox = 0.0

            # Check what the IoU is with regards to the other submitted similar boxes.
            for k in range(0, len(subs)):
                if j != k:
                    if len(subs[j].objects) == len(subs[k].objects):
                        iouBox += IoUboxes(subs[j].objects[i], subs[k].objects[i])
                    else:
                        return None

            # Add the box and the mean IoU to arrays.
            boxes.append(subs[j].objects[i])
            miouBoxes.append(iouBox/(len(subs) - 1))

        # Take the mean box of the boxes with an mean IoU of above the threshold which is in all cases 0.0.
        majorityBoxes.append(MeanBox(BoxesAbovethreshold(boxes, miouBoxes, threshold)))

    # Return as a combined submission.
    return CombinedSub(hits, workerIDs, name, majorityBoxes)