from Helpers.utils import MeanWeightedBox, GetWorker, IoUboxes
from Helpers.objects import Worker, CombinedSub
from collections import defaultdict

# Perform the rejecting workers method on a batch,
#
# with minimal submissions to be able to be considered malicious minSub,
# minimal level to be considered maliciousness minMal,
# threshold to consider a box correct cthreshold,
# threshold for probability that the assignment is correct and no more workers are needed pthreshold
# and if the method is the plus version where the value naive would be false.
def RejectWorkersBatch(batch, minSub, minMal, cthreshold, pthreshold, naive):
    results = []
    workers = []
    totalWorkers = 0

    # Order the submission in groups with the same image.
    subTypes = defaultdict(list)
    for sub in batch.subs:
        subTypes[sub.name].append(sub)

    # For all distinct groups of submissions.
    for subs in subTypes.values():

        # Perform the rejecting workers method and add the workers that where involved.
        result, workers, nWorkers = RejectWorkersSingle(subs, minSub, minMal, cthreshold, pthreshold, workers, naive)
        results.append(result)
        totalWorkers += nWorkers

    # Return resulting boxes and the amount tasks performed by a worker.
    return results, totalWorkers


# Perform the rejecting workers method on a single image,
#
# with minimal submissions to be able to be considered malicious minSub,
# minimal level to be considered maliciousness minMal,
# threshold to consider a box correct cthreshold,
# threshold for probability that the assignment is correct and no more workers are needed pthreshold
# and if the method is the plus version where the value naive would be false.
def RejectWorkersSingle(subs, minSub, minMal, cthreshold, pthreshold, workers, naive):
    boxes, workerIDs, hits = [], [], []
    name = ""
    nWorkers = 0

    # For all submissions made on the image.
    for sub in subs:
        # Add info for the combined submission object.
        workerIDs.append(sub.workerID)
        hits.append(sub.hit)
        name = sub.name

        # Add new workers, that are not in the list, to the list of workers.
        flag = False
        for worker in workers:
            if worker.workerID == sub.workerID:
                flag = True
                break
        if not flag:
            workers.append(Worker(sub.workerID, []))

    # For all boxes.
    for i in range(0, len(subs[0].objects)):

        # Get the proposed bounding boxes with their weights and the minimal number of workers involved.
        totalCredits, pBoxes, weights, workersInvolved = minWorkersForCertainty(subs, workers, i, pthreshold, naive)

        # If the total amount of credits is not 0 thus some worker(s) where certain.
        if totalCredits != 0:

            # Take the mean weighted box of all proposals.
            box = MeanWeightedBox(pBoxes, weights, sum(weights))

            # Get the certainty of the mean weighted prosed box.
            certainty = getCertainty(subs, workers, box, workersInvolved, i)

            # Add the amount of tasks done by workers.
            nWorkers += len(workersInvolved)

        # When total credits is 0 than all workers are malicious or wrong.
        else:
            # Still do majority vote but than regular unweighted of all workers.
            certainty = 1
            if pBoxes == []:
                pBoxes = [sub.objects[i] for sub in subs]
            box = MeanWeightedBox(pBoxes, [1, 1, 1], 3)
            nWorkers += 3

        # Add the mean weighted box to the results.
        boxes.append(box)

        # For all workers that are involved in this image.
        for w in workersInvolved:
            worker = GetWorker(subs[w].workerID, workers)
            worker.submissions.append(subs[w])

            # Check the IoU value of the final box and the worker proposed box.
            io = IoUboxes(subs[w].objects[i], box)

            # If IoU is above the threshold than the worker was correct, else he was wrong.
            if io < cthreshold:
                worker.negativePoints += certainty
            else:
                worker.correctAnswers += 1.0

            # If the worker has made more submissions than the minimal threshold, and has a higher maliciousness value,
            # then the worker is considered malicious and wont be able to participate in other assignments.
            if worker.getSubmissionsMade() > minSub and worker.getMalicious() > minMal:
                worker.isMalicious = True

    # Return the combined sub, the workers that participated and the number of tasks done by the workers.
    return CombinedSub(hits, workerIDs, name, boxes), workers, nWorkers

# Get the certainty that the box is correct.
def getCertainty(subs, workers, box, workersInvolved, i):
    certainty = 0.0
    totCred = 0.0

    # For all workers that are involved.
    for w in workersInvolved:
        # Get the worker.
        oWorker = GetWorker(subs[w].workerID, workers)
        # Get the certainty of that worker that the box is correct.
        certainty += IoUboxes(subs[w].objects[i], box) * oWorker.getCredits()
        # Add the amount of credit of the worker to the total.
        totCred += oWorker.getCredits()

    # Return the chance that the box is correct.
    if totCred > 0.0:
        return certainty / totCred
    else:
        return 0.0

# Get certainty that the box of a worker is correct.
# When compared by the boxes of other workers.
def getCertaintyOthers(subs, workers, i, j, l):

    certaintyW = 0.0
    newCredits = 0

    # For the range of workers to use.
    for k in range(0, l):

        # Check that the submissions have the same length.
        if len(subs[j].objects) != len(subs[k].objects):
            continue

        # Get the other worker
        oWorker = GetWorker(subs[k].workerID, workers)

        # Check that the current worker is not the worker himself.
        if j != k:
            # Check that the worker is not malicious.
            if not oWorker.isMalicious:
                # Get the certainty of the worker.
                certaintyW += IoUboxes(subs[j].objects[i], subs[k].objects[i]) * oWorker.getCredits()
                newCredits += oWorker.getCredits()

    # Return the certainty of the other workers and the amount of total credit those workers have.
    return certaintyW, newCredits

# Calculate the minimal amount of workers needed and their box proposal weights.
def minWorkersForCertainty(subs, workers, i, pthreshold, naive):
    totalCredits = 0
    pBoxes, weights, workersInvolved  = [], [], []

    # For all possible number of workers.
    for l in range(min(2, len(subs)), len(subs)+1):
        totalCredits = 0
        pBoxes, weights, workersInvolved = [], [], []

        # For all workers in this set of workers.
        for j in range(0, l):
            # Check if there are at least 2 workers who made a submissions.
            if len(subs) <= 1:
                continue

            # Check if there exists a submission with less boxes than this one.
            if len(subs[j].objects) <= i:
                continue

            # Get the current worker.
            worker = GetWorker(subs[j].workerID, workers)

            # If the worker is not malicious.
            if not worker.isMalicious:
                workersInvolved.append(j)

                # Get certainty of workers box in regard to the other workers in the set.
                certaintyW, newCredits = getCertaintyOthers(subs, workers, i, j, l)
                totalCredits += newCredits

                # If not all workers are malicious
                if totalCredits != 0:
                    # Check if the normal or the plus method is used.
                    if naive:
                        # Use the normal method.
                        certaintyW = worker.getCredits() / totalCredits
                    else:
                        # Use the plus method.
                        certaintyW = (certaintyW / totalCredits) * worker.getCredits()

                # All other workers are malicious
                else:
                    certaintyW = 1.0

                weights.append(certaintyW)
                pBoxes.append(subs[j].objects[i])

            # Check if more than 1 worker is involved.
            if len(workersInvolved) > 1:
                # Get the mean weighted box and the certainty.
                box = MeanWeightedBox(pBoxes, weights, sum(weights))
                certainty = getCertainty(subs, workers, box, workersInvolved, i)

                # If the certainty is above the minimal threshold than return.
                if certainty > pthreshold:
                    return totalCredits, pBoxes, weights, workersInvolved

    # Return if the certainty threshold is never reached.
    return totalCredits, pBoxes, weights, workersInvolved