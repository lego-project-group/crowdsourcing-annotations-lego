from Helpers.utils import FindRow, ClosestBoxToPoint, PointInBox, DistPoint
from Helpers.objects import CombinedSub
import csv

# Use the decomposition method on a batch of point labeling and box drawings.
def BatchDecomposition(pBatch, bBatch):
    combinedSubs, totalDistances = [], []

    # Open the debug file for writing new assignments when the submitted assignment is wrong.
    with open("Data\\Temp\\debug.csv", mode='w+', newline='') as debug_file:
        # Write the header of the debug file.
        writer = csv.DictWriter(debug_file, ["Name","Labels","Blocks","Counts","Image"])
        writer.writeheader()

        # For every point drawing submission in the batch.
        for pSub in pBatch.subs:

            # Find the corresponding box task.
            bSub = FindRow(bBatch.subs, pSub.name)

            # Combine the box and point drawing tasks.
            combinedSub, totalDistance = Decomposition(pSub, bSub)

            # If the box and point drawing tasks can be successfully combined add them to the output.
            if combinedSub != None:
                combinedSubs.append(combinedSub)
                totalDistances.append(totalDistance)
            # When they cant be successfully combined write a new assignment for crowdsourcing to the debug file.
            else:
                row = {}
                row["Name"] = pSub.name
                row["Labels"] = str(pSub.hit.blocks)
                row["Blocks"] = str(pSub.hit.labels)
                row["Counts"] = str(pSub.hit.counts)
                row["Image"] = pSub.hit.image.url
                writer.writerow(row)

    # Return the successfully combined subs and the distances of the center
    # of the drawn boxes and the point for labeling.
    return combinedSubs, totalDistances

# Decomposition method for a single image.
def Decomposition(pSub, bSub):
    totalDistance = 0
    boxes = bSub.objects

    # For all points placed in the labeling task.
    for point in pSub.objects:

        # Find the corresponding box to the point.
        box = ClosestBoxToPoint(point, boxes)

        # Check if the point is in the box.
        inBox = PointInBox(point, box)

        # If it is in the box label the box with that label.
        if inBox:
            box.label = point.label
            box.score = DistPoint(point, box)
            totalDistance += DistPoint(point, box)

        # If the point is not in the box the assignment has to be done over.
        else:
            return None, -1

    # Calculate the mean total distance between the boxes and points.
    meanTotalDistance = totalDistance/len(pSub.objects)

    # Return as combined sub with total mean distance.
    return CombinedSub([pSub.hit, bSub.hit], [pSub.workerID, bSub.workerID], pSub.name, boxes), meanTotalDistance