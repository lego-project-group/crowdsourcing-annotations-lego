from Helpers.utils import MeanBox
from Helpers.objects import CombinedSub
from collections import defaultdict

# Run the normal majority vote for a batch.
def MajorityVoteBatchNaive(batch):
    results = []

    # Order the submissions in groups with submissions on the same task.
    subTypes = defaultdict(list)
    for sub in batch.subs:
        subTypes[sub.name].append(sub)

    # For all distinct submissions do majority vote.
    for subs in subTypes.values():

        # When there are 1 or less submissions than skip this submission.
        if len(subs) <= 1:
            continue

        # Do majority vote and add to the results when not none.
        res = MajorityVoteSingleNaive(subs)
        if res != None:
            results.append(res)

    return results

# Run the normal majority for a single image.
def MajorityVoteSingleNaive(subs):
    majorityBoxes = []
    workerIDs = []
    hits = []
    name = ""
    obLen = -1

    # For all submissions on this single image.
    for sub in subs:
        # Add info for creating a combined sub object.
        workerIDs.append(sub.workerID)
        hits.append(sub.hit)
        name = sub.name

        # Check that the submissions have the same length of boxes.
        if obLen != -1 and obLen != len(sub.objects):
            return None

        obLen = len(sub.objects)

    # For all boxes.
    for i in range(0, len(subs[0].objects)):

        # Put all similar boxes in an array.
        boxes = []
        for j in range(0, len(subs)):
            boxes.append(subs[j].objects[i])

        # Append the mean of the boxes to the results.
        majorityBoxes.append(MeanBox(boxes))

    # Return as a combined submission.
    return CombinedSub(hits, workerIDs, name, majorityBoxes)