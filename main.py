from Helpers.dataManager import ReadRows, ReadBatch, AddQualification, ExtendedSubsToCSV, CombinedSubsToCSV
from Helpers.figureManager import qualityBoxesFigure, qualityImagesFigure, qualityNBricksFigure, table2Figure
from Helpers.utils import *
from Methods.rejectWorkers import RejectWorkersBatch
from Methods.majorityVote import MajorityVoteBatchNaive
from Methods.majorityVotePlus import MajorityVoteBatch
from Methods.decomposition import BatchDecomposition

# Read all rows from the input files to provide more information for the annotations.
roots = ["Input\\Datasets\\Berend", "Input\\Datasets\\Hiba", "Input\\Datasets\\David"]
rows = ReadRows(roots, False)
rows.extend(ReadRows(roots, False, extra="2"))

# Read the ground-truth boxes.
truth = ReadBatch('Groundtruths.csv', "box")
truth2 = ReadBatch('Groundtruths2.csv', "box")
truth.subs.extend(truth2.subs)

# Perform the majority vote and majority vote plus methods.
batch = ReadBatch('realMVCombined.csv', "box")
resultsMV = MajorityVoteBatchNaive(batch)
resultsMVP = MajorityVoteBatch(batch, 0.0)

# Perform the rejecting workers and the rejecting workers plus method.
resultsRW, nRWWorkers = RejectWorkersBatch(batch, 15, 0.8, 0.6, 0.9, True)
resultsRWP, nRWPWorkers = RejectWorkersBatch(batch, 15, 0.8, 0.6, 0.9, False)
nBatch = BatchObjectsCount(batch.subs)/3

# Perform the decomposing tasks method.
batchPoints = ReadBatch('realDTCombinedTask1.csv', "keypoints")
batchBoxes = ReadBatch('realDTCombinedTask2.csv', "unlabeled_box")
resultsDT, totalDist = BatchDecomposition(batchPoints, batchBoxes)

# Add qualification to the workers that participated in drawing the boxes such that they cant label them.
AddQualification(batchBoxes.workers, "workers1.csv")

# Save the predictions from the methods to folders.
ExtendedSubsToCSV(truth.subs, rows, "realGroundtruths")
ExtendedSubsToCSV(batch.subs, rows, "realCombined_NP")
CombinedSubsToCSV(resultsMV, rows, "realCombined_MV")
CombinedSubsToCSV(resultsMVP, rows, "realCombined_MVP")
CombinedSubsToCSV(resultsRW, rows, "realCombined_RW")
CombinedSubsToCSV(resultsRWP, rows, "realCombined_RWP")
CombinedSubsToCSV(resultsDT, rows, "realCombined_DT")

# Calculate the IoU values of all boxes in the batches from the methods.
mvIoUBatch, mvpIoUBatch = IoUBoxBatch(resultsMV, truth.subs), IoUBoxBatch(resultsMVP, truth.subs)
rwIoUBatch, rwpIoUBatch = IoUBoxBatch(resultsRW, truth.subs), IoUBoxBatch(resultsRWP, truth.subs)
dtIoUBatch = IoUBoxBatch(resultsDT, truth.subs)

# Count the number of boxes that each method uses.
mvBoxesTotal, mvpBoxesTotal = IoUObjectsCount(mvIoUBatch), IoUObjectsCount(mvpIoUBatch)
rwBoxesTotal, rwpBoxesTotal = IoUObjectsCount(rwIoUBatch), IoUObjectsCount(rwpIoUBatch)
dtBoxesTotal = IoUObjectsCount(dtIoUBatch)

# Get the IoUs of the methods above certain thresholds.
thresholds = [x * 0.1 for x in range(0, 11)]

MVBoxes, MVImages = GetAboveThresholds(thresholds, mvIoUBatch, mvBoxesTotal)
MVPBoxes, MVPImages = GetAboveThresholds(thresholds, mvpIoUBatch, mvpBoxesTotal)
RWBoxes, RWImages = GetAboveThresholds(thresholds, rwIoUBatch, rwBoxesTotal)
RWPBoxes, RWPImages = GetAboveThresholds(thresholds, rwpIoUBatch, rwpBoxesTotal)
DTBoxes, DTImages = GetAboveThresholds(thresholds, dtIoUBatch, dtBoxesTotal)

# Create rows for Table 2.
cDT, cRW, cRWP, cMV, cMVP = 0.1 * (1771/ 1534),  0.1 * (nRWWorkers/nBatch), 0.1 * (nRWPWorkers/nBatch), 0.3, 0.3

r1 = [round(MeanIoUBatch(dtIoUBatch), 3), round(MeanIoUBatch(rwIoUBatch), 3), round(MeanIoUBatch(rwpIoUBatch), 3), round(MeanIoUBatch(mvIoUBatch), 3), round(MeanIoUBatch(mvpIoUBatch), 3)]
r2 = [round(DTImages[5], 3), round(RWImages[5], 3), round(RWPImages[5], 3), round(MVImages[5], 3), round(MVPImages[5], 3)]
r3 = ["$" + str(round(cDT, 2)), "$" + str(round(cRW, 2)), "$" + str(round(cRWP, 2)), "$" + str(round(cMV, 2)), "$" + str(round(cMVP, 2))]
r4 = [round(MeanIoUBatch(dtIoUBatch)/ cDT, 3), round(MeanIoUBatch(rwIoUBatch)/ cRW, 3), round(MeanIoUBatch(rwpIoUBatch)/ cRWP, 3), round(MeanIoUBatch(mvIoUBatch)/ cMV, 3), round(MeanIoUBatch(mvpIoUBatch)/ cMVP, 3)]
r5 = [round(DTImages[5] / cDT, 3), round(RWImages[5] / cRW, 3), round(RWPImages[5] / cRWP, 3), round(MVImages[5] / cMV, 3), round(MVPImages[5] / cMVP, 3)]

# Generate the figures.
qualityNBricksFigure([MeanIoUBatchDependentNBlocks(mvIoUBatch, 13), MeanIoUBatchDependentNBlocks(mvpIoUBatch, 13),
                      MeanIoUBatchDependentNBlocks(rwIoUBatch, 13), MeanIoUBatchDependentNBlocks(rwpIoUBatch, 13),
                      MeanIoUBatchDependentNBlocks(dtIoUBatch, 13)])
qualityImagesFigure(thresholds, [MVImages, MVPImages, RWImages, RWPImages, DTImages])
qualityBoxesFigure(thresholds, [MVBoxes, MVPBoxes, RWBoxes, RWPBoxes, DTBoxes])
table2Figure([r1, r2, r3, r4, r5])
