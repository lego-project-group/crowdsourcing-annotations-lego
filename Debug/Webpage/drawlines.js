var index = 0;
var files;

document.onkeydown = arrowKey;

function arrowKey(e) {

	e = e || window.event;

	if (e.keyCode == '37') {
	   previous();
	}
	else if (e.keyCode == '39') {
	   next();
	}

}

function previous(){
	readFile(files[index - 1], index - 1);
	console.log(index)
}

function next(){
	readFile(files[index + 1], index + 1);
	console.log(index)
}

function getRandomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
	color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

function emptyLegend(){
  var div = document.getElementById("legend");
  while (div.firstChild) {
	div.removeChild(div.lastChild);
  }
}

function addLegendItems(lines){
  var div = document.createElement("div");
  div.classList.add("container");

	for(var i = 0;i < lines.length-3;i++){
		var item = lines[i+2].split(',')[0];
        if (item == "None"){
            return
        }

		var text = document.createElement("div");
		text.innerHTML = item;

		if (i % 3 == 0){
			text.classList.add("left");
		} else if  (i % 3 == 1){
			text.classList.add("middle");
		} else {
			text.classList.add("right");
		}

		if (i < 3){
			text.classList.add("topP");
		} else if (i < 6){
			text.classList.add("middleP");
		} else if (i < 9){
			text.classList.add("bottomP");
		} else {
			text.classList.add("lastP");
		}

		var img = document.createElement("img");
		img.src = "https://www.lego.com/service/bricks/5/2/" + item;
		img.classList.add("imag");
		img.width = 70
		img.height = 70

		div.appendChild(text);
		div.appendChild(img);
  }

  var src = document.getElementById("legend");
  src.appendChild(div)

  var title = document.getElementById("title");
  title.innerHTML = lines[1].split(',')[0];
}

function clearCanvas() {
	var c = document.getElementById("myCanvas");
	var ctx = c.getContext("2d");
	ctx.clearRect(0, 0, c.width, c.height);
}

function readFile(file, i) {
	offset = 0;
	index = i;
	var reader = new FileReader();
	var c = document.getElementById("myCanvas");
	var ctx = c.getContext("2d");
	var clear = document.getElementById("clear").checked;

	if(clear){
		clearCanvas();
		emptyLegend();
	}

	reader.addEventListener('load', function(e) {
		var text = e.target.result;
		var lines = text.split('\n');

		var colorLine = getRandomColor();
		var imgInf = lines[0].split(',');
		document.getElementById("imag").src = imgInf[0];

		if (c.width != imgInf[1] || c.height != imgInf[2]){
			c.width = imgInf[1];
			c.height = imgInf[2];
		}

		ctx.font = "20px Arial";
		offset += 25;

		addLegendItems(lines);

		for(var i = 2;i < lines.length;i++){
			var box = lines[i].split(',');

			ctx.beginPath();
			ctx.fillStyle = "white";
			ctx.strokeStyle = 'black';
			ctx.lineWidth = 1;
			ctx.fillText(box[0], box[1], box[2] - offset);
			ctx.strokeText(box[0], box[1], box[2] - offset);
			ctx.fill();
			ctx.stroke();

			ctx.beginPath();
			ctx.lineWidth = 2;
			ctx.strokeStyle = colorLine;
			ctx.moveTo(box[1], box[2]);
			ctx.lineTo(box[1], box[4]);
			ctx.lineTo(box[3], box[4]);
			ctx.lineTo(box[3], box[2]);
			ctx.lineTo(box[1], box[2]);
			ctx.stroke();

		}
	});

	reader.readAsText(file);
}

window.onload=function(){
	var c = document.getElementById("myCanvas");
	var offset = 0;

	document.querySelector("#read-button1").addEventListener('click', function() {
		var file = document.querySelector("#file-input1").files[0];
		readFile(file, 0);
	});

	document.querySelector("#read-button2").addEventListener('click', function() {
		var file = document.querySelector("#file-input2").files[0];
		var reader = new FileReader();
		var ctx = c.getContext("2d");

		reader.addEventListener('load', function(e) {
			var text = e.target.result;
			var lines = text.split('\n');

			var colorLine = getRandomColor()
			ctx.fillStyle = "Maroon";
			ctx.font = "30px Arial";
			ctx.lineWidth = 3;
			offset += 25
			for(var i = 2;i <= lines.length;i++){
				var box = lines[i].split(',');

				ctx.fillText(box[0], box[1], box[2]);

				ctx.beginPath();
				ctx.moveTo(box[1], box[2]);
				ctx.lineTo(box[3], box[4]);
				ctx.strokeStyle = colorLine;
				ctx.stroke();
			}
		});

		reader.readAsText(file);
	});

	document.querySelector("#read-button3").addEventListener('click', function() {

		var inp = document.getElementById('files');
		files = inp.files;

		for (var i = 0; i < inp.files.length; ++i) {
			var file = inp.files.item(i);

			var button = document.createElement("button");
			button.innerHTML = file.name;
			button.value = i;

			function reply_click(){
			    console.log(this.value)
				readFile(files[this.value], this.value);
			}
			button.onclick = reply_click

			var body = document.getElementById("filebuttons");
			body.appendChild(button);
		}

	});
}