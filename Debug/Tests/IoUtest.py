from Helpers.utils import *

b1 = Box(1,1,3,2, "", 0)
b2 = Box(1,1,3,2, "", 0)

iou1 = IoUboxes(b1, b2)

print("Test full overlap 1/1")
print("IOU1: ", iou1)
print("-----------------------------------------------")


b1 = Box(1,1,2,2, "", 0)
b2 = Box(2,1,3,2, "", 0)

iou1 = IoUboxes(b1, b2)


b1 = Box(1,1,3,2, "", 0)
b2 = Box(1,1,3,2, "", 0)

iou1 = IoUboxes(b1, b2)

print("Test full overlap 1/1")
print("IOU1: ", iou1)
print("-----------------------------------------------")


b1 = Box(1,1,2,2, "", 0)
b2 = Box(2,1,3,2, "", 0)

iou1 = IoUboxes(b2, b1)


print("Test no overlap 0/0")
print("IOU1: ", iou1)
print("-----------------------------------------------")

b1 = Box(1,1,2,2, "", 0)
b2 = Box(2,1,3,2, "", 0)

iou1 = IoUboxes(b2, b1)

print("Test no overlap 0/0")
print("IOU1: ", iou1)
print("-----------------------------------------------")


b1 = Box(1,1,3,2, "", 0)
b2 = Box(2,1,3,2, "", 0)

iou1 = IoUboxes(b1, b2)

print("Test half overlap 1/2")
print("IOU1: ", iou1)
print("-----------------------------------------------")


b1 = Box(1,1,3,2, "", 0)
b2 = Box(2,1,3,2, "", 0)

iou1 = IoUboxes(b2, b1)

print("Test half overlap 1/2")
print("IOU1: ", iou1)
print("-----------------------------------------------")


b1 = Box(0,0,2,2, "", 0)
b2 = Box(1,0,3,2, "", 0)

iou1 = IoUboxes(b1, b2)

print("Test half overlap and half not 1/3")
print("IOU1: ", iou1)
print("-----------------------------------------------")


b1 = Box(0,0,2,2, "", 0)
b2 = Box(1,0,3,2, "", 0)

iou1 = IoUboxes(b2, b1)

print("Test half overlap and half not 1/3")
print("IOU1: ", iou1)
print("-----------------------------------------------")


b1 = Box(0,0,2,2, "", 0)
b2 = Box(1,1,3,3, "", 0)

iou1 = IoUboxes(b1, b2)

print("Test quarter overlap and third not 1/7")
print("IOU1: ", iou1)
print("-----------------------------------------------")

b1 = Box(0,0,2,2, "", 0)
b2 = Box(1,1,3,3, "", 0)

iou1 = IoUboxes(b2, b1)

print("Test quarter overlap and third not 1/7")
print("IOU1: ", iou1)
print("-----------------------------------------------")


