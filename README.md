# Crowdsourcing Annotations Lego

Code for the paper: "Evaluating Methods for Improving Crowdsourced Annotations of Images Containing Lego Bricks" 2020 



The images used can be found on https://surfdrive.surf.nl/files/index.php/s/B7PEqFZSZZBPcW8

The code for the methods from the paper can be found in the folder Methods.

Data folder contain the results of the crowdsourcing in Data/Input/Crowdsourcing and the outputs of the methods in Data/Ouput/realCombined_[method].

Running the main.py python file will run all crowdsourcing improvement methods from the paper, and generate all figures and Table 2 from the results. 

